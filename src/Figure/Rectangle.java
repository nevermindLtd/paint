package Figure;

import java.awt.Color;

public class Rectangle extends Figure {
	protected int width, height;

	public Rectangle(int x, int y, int width, int height, Color c) {
		super(new Point(x,y),c);
		this.width = width;
		this.height = height;
	}

	/**
	 * @return the x
	 */
	public int getX() {
		return origine.x;
	}

	/**
	 * @return the y
	 */
	public int getY() {
		return origine.y;
	}

	/**
	 * @return the width
	 */
	public int getWidth() {
		return width;
	}

	/**
	 * @return the height
	 */
	public int getHeight() {
		return height;
	}
}
