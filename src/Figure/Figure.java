package Figure;

import java.awt.Color;

public class Figure {
	protected Point origine;
	protected Color color;

	public Figure(Point origine ,Color color) {
		this.color = color;
		this.origine = origine;
	}

	/**
	 * @return the color
	 */
	public Color getColor() {
		return color;
	}

	/**
	 * @param color
	 *            the color to set
	 */
	protected void setColor(Color color) {
		this.color = color;
	}

	/**
	 * @return the origine
	 */
	public Point getOrigine() {
		return origine;
	}

	/**
	 * @param origine the origine to set
	 */
	protected void setOrigine(Point origine) {
		this.origine = origine;
	}

}
