package first_IHM;

import java.awt.Color;
import java.awt.Graphics;
import java.util.ArrayList;

import javax.swing.JPanel;

import Figure.Figure;

public class Dessin extends JPanel {
	ArrayList<Figure> list;
	Color c;
	String nomFigure;

	public Dessin() {
		super();
	}

	@Override
	public void paintComponent(Graphics g) {
		super.paintComponent(g);
		setBackground(Color.WHITE);
		g.setColor(Color.RED);
		g.fillRect(100, 100, 10, 10);
	}
}
