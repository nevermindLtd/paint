package first_IHM;

import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.KeyStroke;

public class Fenetre extends JFrame implements ActionListener {
	static final long serialVersionUID = 712286517053415187L;
	private Dessin dessin;

	Fenetre(String titre, int width, int height) {
		super(titre);
		this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(width, height);
		this.setLocation(0, 0);

		Container contentPane = getContentPane();
		createMenuBar();
		createEditZone(contentPane);
		createBottomBar(contentPane);

		setVisible(true);
	}

	private void createMenuBar() {
		JMenuBar m = new JMenuBar();

		JMenu menuFichier = new JMenu("Fichier");
		JMenuItem fichierOuvrir = new JMenuItem("Ouvrir");
		JMenuItem fichierNouveau = new JMenuItem("Nouveau");
		JMenuItem fichierEnregistrer = new JMenuItem("Enregistrer");
		JMenuItem fichierQuitter = new JMenuItem("Quitter");
		fichierOuvrir.setActionCommand("OPEN");
		fichierNouveau.setActionCommand("NEW");
		fichierEnregistrer.setActionCommand("SAVE");
		fichierQuitter.setActionCommand("QUIT");
		fichierOuvrir.addActionListener(this);
		fichierNouveau.addActionListener(this);
		fichierEnregistrer.addActionListener(this);
		fichierQuitter.addActionListener(this);
		menuFichier.add(fichierOuvrir);
		menuFichier.add(fichierNouveau);
		menuFichier.add(fichierEnregistrer);
		menuFichier.add(fichierQuitter);
		// raccourcis
		fichierOuvrir.setAccelerator(
				KeyStroke.getKeyStroke('O', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		fichierNouveau.setAccelerator(
				KeyStroke.getKeyStroke('N', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		fichierEnregistrer.setAccelerator(
				KeyStroke.getKeyStroke('S', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		fichierQuitter.setAccelerator(
				KeyStroke.getKeyStroke('Q', Toolkit.getDefaultToolkit().getMenuShortcutKeyMask(), false));
		m.add(menuFichier);

		JMenu menuAPropos = new JMenu("A Propos");
		menuAPropos.add(new JMenuItem("Auteurs"));
		m.add(menuAPropos);
		setJMenuBar(m);
	}

	private void createEditZone(Container contentPane) {
		dessin = new Dessin();
		contentPane.add(dessin, "Center");
	}

	private void createBottomBar(Container contentPane) {
		JPanel bottomBar = new JPanel();
		bottomBar.setLayout(new FlowLayout());
		JPanel bottomBarCouleurs = new JPanel();
		bottomBarCouleurs.setLayout(new GridLayout(2, 4));
		JPanel bottomBarFormes = new JPanel();
		bottomBarFormes.setLayout(new GridLayout(2, 2));

		JButton buttonNoir = new JButton("Noir");
		JButton buttonRouge = new JButton("Rouge");
		JButton buttonVert = new JButton("Vert");
		JButton buttonBleu = new JButton("Bleu");
		JButton buttonJaune = new JButton("Jaune");
		JButton buttonRose = new JButton("Rose");
		JButton buttonMagenta = new JButton("Magenta");
		JButton buttonOrange = new JButton("orange");
		buttonNoir.setBackground(Color.BLACK);
		buttonRouge.setBackground(Color.RED);
		buttonVert.setBackground(Color.GREEN);
		buttonBleu.setBackground(Color.BLUE);
		buttonJaune.setBackground(Color.YELLOW);
		buttonRose.setBackground(Color.PINK);
		buttonMagenta.setBackground(Color.MAGENTA);
		buttonOrange.setBackground(Color.ORANGE);
		buttonNoir.setActionCommand("BLACK");
		buttonRouge.setActionCommand("RED");
		buttonVert.setActionCommand("GREEN");
		buttonBleu.setActionCommand("BLUE");
		buttonJaune.setActionCommand("YELLOW");
		buttonRose.setActionCommand("PINK");
		buttonMagenta.setActionCommand("MAGENTA");
		buttonOrange.setActionCommand("ORANGE");
		buttonNoir.addActionListener(this);
		buttonRouge.addActionListener(this);
		buttonVert.addActionListener(this);
		buttonBleu.addActionListener(this);
		buttonJaune.addActionListener(this);
		buttonRose.addActionListener(this);
		buttonMagenta.addActionListener(this);
		buttonOrange.addActionListener(this);
		bottomBarCouleurs.add(buttonNoir);
		bottomBarCouleurs.add(buttonRouge);
		bottomBarCouleurs.add(buttonVert);
		bottomBarCouleurs.add(buttonBleu);
		bottomBarCouleurs.add(buttonJaune);
		bottomBarCouleurs.add(buttonRose);
		bottomBarCouleurs.add(buttonMagenta);
		bottomBarCouleurs.add(buttonOrange);

		JButton buttonElipse = new JButton("Elipse");
		JButton buttonCercle = new JButton("Cercle");
		JButton buttonCarre = new JButton("Carré");
		JButton buttonRectangle = new JButton("Rectangle");
		buttonElipse.setActionCommand("ELIPSE");
		buttonCercle.setActionCommand("CIRCLE");
		buttonCarre.setActionCommand("SQUARE");
		buttonRectangle.setActionCommand("RECTANGLE");
		buttonElipse.addActionListener(this);
		buttonCercle.addActionListener(this);
		buttonCarre.addActionListener(this);
		buttonRectangle.addActionListener(this);
		bottomBarFormes.add(buttonElipse);
		bottomBarFormes.add(buttonCercle);
		bottomBarFormes.add(buttonCarre);
		bottomBarFormes.add(buttonRectangle);

		bottomBar.add(bottomBarCouleurs);
		bottomBar.add(bottomBarFormes);
		contentPane.add(bottomBar, "South");
	}

	@Override
	public void actionPerformed(ActionEvent e) {
		System.out.println(e.getActionCommand());
		switch(e.getActionCommand()){
		case "BLACK":
			dessin.setBackground(Color.BLACK);
			break;
		case "RED":
			dessin.setBackground(Color.RED);
			break;
		case "GREEN":
			dessin.setBackground(Color.GREEN);
			break;
		case "BLUE":
			dessin.setBackground(Color.BLUE);
			break;
		case "YELLOW":
			dessin.setBackground(Color.YELLOW);
			break;
		case "PINK":
			dessin.setBackground(Color.PINK);
			break;
		case "MAGENTA":
			dessin.setBackground(Color.MAGENTA);
			break;
		case "ORANGE":
			dessin.setBackground(Color.ORANGE);
			break;
		case "OPEN":
			System.out.println("OPEN - TODO");// TODO
			break;
		case "NEW":
			System.out.println("NEW - TODO");// TODO
			break;
		case "SAVE":
			System.out.println("SAVE - TODO");// TODO
			break;
		case "QUIT":
			System.exit(0);
			break;
			
		}
	}

}
